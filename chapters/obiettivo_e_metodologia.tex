\chapter{Obiettivo e Metodologia}
In questo capitolo descriveremo il nostro approccio al problema. 
\newline

Per prima cosa descriveremo 
quale problema abbiamo deciso di provare a risolvere, illustreremo poi il nostro metodo per
la sua risoluzione, dalle fasi preliminari a quelle conclusive. 

\section{Obiettivo}
Lo scopo di questo studio \`e quello di ottimizzare l'\textit{Area Under ROC Curve} degli algoritmi 
che abbiamo precedentemente illustrato senza, tuttavia, tralasciare le altre metriche (questo sia per completezza che per continuit\`a con la letteratura)
\newline

Ci \`e parso interessante approfondire tale aspetto poich\'e si \`e notato che spesso algoritmi con 
punteggi molto alti in quanto a rilevamenti tendono ad avere una AUC molto bassa. 
\newline

Questo si 
traduce in algoritmi che ottengono buoni risultati solo a seguito di una lunga ed accurata fase di 
configurazione.  
\newline

\textbf{Vogliamo, in altre parole, ottenere dalla combinazione di algoritmi con punteggi non ottimali 
risultati equiparabili a quelli con punteggi ottimali, senza tuttavia soffrire della stessa 
difficolt\`a di configurazione e prestando un occhio di riguardo al costo computazionale.} 
\section{Strategie di combinazione} 
Innanzitutto abbiamo deciso come combinare gli algoritmi (di una stessa classe di 
complessit\`a) ed abbiamo considerato le seguenti opzioni basandosi su diverse possibilit\`a di 
esecuzione degli stessi: 

\begin{itemize}
  \item \textbf{parallelo}: 
    
     se gli algoritmi vengono eseguiti parallelamente, ognuno pu\`o avere 
  una sua opinione per quel che riguarda la natura del dato. Possiamo quindi provare a combinare 
  i loro risultati mediante l'utilizzo di un aggiudicatore \cite{24}. Questo pu\`o basarsi su 
  funzioni di valutazione diverse:
  
  \begin{itemize}
  	\item \textbf{2-su-3}: questo classico metodo di votazione ci \`e parso adeguato in quanto sembra 
  	fornire un buon bilanciamento in quanto all'introduzione di falsi positivi/negativi, a differenza 
  	di votazioni 1-su-3 (che introduce falsi positivi in quanto basta che un algoritmo consideri il 
  	dato anomalo affinch\`e sia riconosciuto tale) o 3-su-3 (nel quale vengono introdotti falsi 
  	negativi poich\'e se due algoritmi credono un dato anomalo ed il terzo, sbagliando, lo considera 
  	normale, questo viene considerato normale). 
  	
  	\item \textbf{media ponderata}: 
    
     abbiamo scelto tale metodo poich\'e i pesi utilizzati nella 
  	media dei risultati degli algoritmi ci permettono di dare pi\`u o meno importanza ad un algoritmo 
  	in base alle sue caratteristiche; i pesi sono scelti in funzione delle metriche trovate in 
  	\cite{25}, in particolare sul \textit{MCC} poich\'e troviamo fornisca un miglior compromesso 
  	tra \textit{precision} e \textit{recall} in confronto con accuracy e F1, come motivato in \cite{27}.
  	
  	In particolare abbiamo deciso di dare al 'migliore' algoritmo un peso doppio rispetto 
  	agli altri due.
  	
  	\begin{figure}[!h]
  		\centering
  		\includegraphics[scale=0.3]{images/adjudicator.png}
  		\caption{algoritmi in parallelo con aggiudicatore o votatore a seguito}
  	\end{figure}
  \end{itemize}
  \item \textbf{serializzazione con sentinelle di n-con-m algoritmi}: 
    
     quest'ultimo caso 
  e' leggermente pi\`u complesso del precedente poich\'e tenta di unire esecuzione seriale e 
  parallela come segue:  
  
  \begin{enumerate}
    \item eseguiamo $ n $ algoritmi parallelamente (con una precision bassa, quindi con una 
    tendenza a sollevare falsi positivi)
    \item passiamo i risultati ottenuti ad altri $ m $ algoritmi, anch'essi eseguiti 
    parallelamente    
   	\item infine eseguiamo o un ulteriore algoritmo o un aggiudicatore per unificare 
   	i risultati ottenuti in	un singolo valore binario (dato normale o singolare).
  \end{enumerate}
  
  \begin{figure}[!h]
  		\centering
  		\includegraphics[scale=0.5]{images/n-m.png}
  		\caption{serializzazione con sentinelle di n-con-m algoritmi}
  \end{figure}
  
  \`E imperativo evidenziare alcuni casi particolari che si originano da quest'ultima 
  strategia in quanto in questo studio utilizzeremo una di queste.
  \begin{itemize}
    \item n-con-1:  
    
    in questo caso $ m $ \`e uguale ad uno. Ne consegue che le entit\`a descritte 
    in 2 e 3 degenerano in un'unica entit\`a, in questo caso un solo algoritmo, che sia valuta 
    gli algoritmi eseguiti in 1 sia produce un unico risultato binario.  
        
    In questo studio utilizzeremo la variante 2-con-1 in quanto per ogni categoria (riguardo la 
    complessit\`a) abbiamo solamente tre algoritmi.
    \begin{figure}[!h]
    	\centering
  		\includegraphics[scale=0.5]{images/n-1.png}
  		\caption{serializzazione con sentinelle di n-con-1 algoritmi}
	\end{figure}
    \item 1-con-m:  
    
    a differenza del caso precedente qui non assistiamo ad una riduzione delle 
    fasi che contraddistinguono tale strategia, in quanto, avendo $ m $ algoritmi nell'esecuzione
    della seconda fase, rimane fondamentale avere un'ulteriore entit\`a che ne combini i risultati.
    \begin{figure}[!h]
    	\centering
  		\includegraphics[scale=0.5]{images/1-m.png}
  		\caption{serializzazione con sentinelle di 1-con-m algoritmi}
	\end{figure}
  \end{itemize}
\end{itemize}

\section{Algoritmi}
In seguito abbiamo dovuto decidere gli algoritmi da combinare, scelti in 
funzione della loro complessit\`a:

\newpage
\begin{center}
\begin{table}[!h]
\centering
\begin{tabular}{c c c}
\hline
\hline
\textbf{Algoritmo} 	& \textbf{Famiglia} 	& \textbf{Complessit\`a}				\\
\hline
ABOD 				& Angle Based      		&  $ O(n^{3}) $ \cite{23}   			\\
\hline
FastABOD 			& Angle Based      		&  $ O(n^{2}) $ \cite{23}  				\\
\hline
Isolation Forest    & Classification Based  &  $ O(nt\varphi) $ * \cite{33} 		\\
\hline
One Class SVM       & Classification Based  &  $ O(n^{2}) $ \cite{23}  				\\
\hline
K-Means      		& Clustering Based     	&  $ O(nk) $ **	\cite{23}				\\
\hline
LDCOF      			& Clustering Based     	&  $ O(nk) $ **	\cite{23}				\\
\hline
COF 				& Density Based      	&  $ O(n^{2}) $ \cite{23}  				\\
\hline
LOF 				& Density Based      	&  $ O(n^{2}) $ \cite{23}  				\\ 
\hline
KNN 				& Neighbour Based      	&  $ O(n^{2}) $ \cite{5}				\\	 
\hline
ODIN 				& Neighbour Based      	&  $ O(n^{2}) $ \cite{5}  				\\		 
\hline
HBOS 				& Statistical      		&  $ O(n\log{n}) $ \cite{23}			\\
\hline
rPCA 				& Statistical      		&  $ O(nd^{2}+d^{3}) $ ***  \cite{9}	\\   
\hline
\hline
\end{tabular}
 
    
     
* $t =$ numero di alberi generati, $\varphi =$ numero di nodi per ogni albero generato, ** $k =$ numero di \textit{cluster}, 
*** $d =$ numero di dimensioni
\caption{Complessit\`a degli algoritmi}
\end{table}
\end{center}
\begin{itemize}
  \item \textbf{linearitmica}:  
    
    per rispettare tale condizione ($ O(n\log{n}) $), la scelta \`e ricaduta sulle 
  famiglie \textit{Clustering Based} e \textit{Statistical}, dalle quali sono stati scelti rispettivamente 
  \textit{LDCOF}, \textit{HBOS} e \textit{K-Means}.  
   \newline
    
    Tale selezione \`e stata guidata dalla voglia di ottenere dalla combinazione di questi 
  algoritmi un'ampia copertura per quanto riguarda le tipologie di anomalia.  
    \newline
   
    Tuttavia, dai risultati 
  ottenuti in \cite{23}, sappiamo che nonostante riescano a rilevare \textit{point anomalies} e \textit{collective 
  anomalies}, questi algoritmi hanno difficolt\`a a rilevarne di contestuali;
   
  \item \textbf{quadratica}:  
    
    in questo caso ($ O(n^{2}) $) la scelta \`e molto pi\`u ampia poich\'e la maggior 
  parte degli algoritmi che abbiamo introdotto hanno costo quadratico.  
    \newline
    
    In questa categoria possiamo scegliere algoritmi pi\`u versatili: si \`e infatti 
  scelto di combinare \textit{ODIN} con \textit{COF} e \textit{LOF} (che sono risultati in grado di rilevare anomalie 
  contestuali\cite{23}); gli algoritmi scelti appartengono rispettivamente alle famiglie
  \textit{Neighbour Based} e \textit{Density Based}.
  
\end{itemize}

\section{Metodologia della fase sperimentale}
Abbiamo definito quali algoritmi usare ed i possibli metodi di combinarli, ma non 
abbiamo ancora messo insieme le ipotesi fatte. Andiamo, quindi, a definire le tuple 
di algoritmi e le tecniche per aggregarli.
\newline

Per quanto riguarda gli algoritmi con complessit\`a (al massimo) linearitmica 
abbiamo deciso di testare le seguenti opzioni: 
\begin{itemize}
  \item votazione 2-su-3 tra LDCOF, HBOS e K-Means
  \item media pesata tra LDCOF, HBOS e K-Means 
  \item serializzazione con sentinelle di LDCOF e HBOS con K-Means  
\end{itemize}

Invece, per quel che riguarda gli algoritmi con complessit\`a (al massimo) quadratica: 
\begin{itemize}
  \item votazione 2-su-3 tra ODIN, COF e LOF
  \item media pesata tra ODIN, COF e LOF
  \item serializzazione con sentinelle di ODIN, COF e LOF
  \end{itemize}
  
Per elaborare i risultati di tali esperimenti ci siamo avvalsi delle seguenti risorse: 
i risultati degli algoritmi ottenuti in \cite{25} e \textit{LibreOffice Calc}.
\newline

L'utilizzo dei fogli di calcolo come strumento per effettuare le combinazioni ci ha permesso 
di farlo mediante l'utilizzo di semplici operatori logici ed aritmetici.
\newline

Tale strumento ci ha anche consentito di vedere come ogni istanza 
del dataset venga rilevata dai singoli algoritmi, cos\`i come dalle loro combinazioni.
\newline

Ogni foglio elettronico di calcolo mostra per ogni algoritmo, e per ogni combinazione di questi, il valore delle relative metriche.
\newline

Al fine di valuare i risultati ottenuti abbiamo calcolato le metriche di un algoritmo con ottimi risultati (come visto in \cite{25}), nonostante 
l'elevato costo computazionale, ovvero \textit{Isolation Forest}.
\newline

Andiamo adesso ad elencare nel dettaglio la struttura della tabella (immagine a seguire), per colonne:

\begin{sidewaysfigure}[htp]
	\centering
	\includegraphics[scale=0.3]{images/calc.png}
	\caption{esempio di foglio di calcolo elettronico usato}
\end{sidewaysfigure}

\renewcommand{\labelenumi}{\Alph{enumi}}
\begin{enumerate}
  \item Punteggi del primo algoritmo
  \item Traduzione dei punteggi in valori binari (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Punteggi del secondo algoritmo
  \item Traduzione dei punteggi in valori binari (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Punteggi del terzo algoritmo
  \item Traduzione dei punteggi in valori binari (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Punteggi dell'algoritmo scelto per i confronti con i risultati ottenuti
  \item Traduzione dei punteggi in valori binari (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Eticchetta indicante la vera natura del dato in analisi (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Risultati della combinazione dei tre algoritmi scelti mediante votazione 2-su-3 (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Risultati della combinazione dei tre algoritmi scelti mediante media pesata (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Risultati della combinazione dei tre algoritmi scelti mediante serializzazione con sentinella di primo e terzo algoritmo con il secondo (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Risultati della combinazione dei tre algoritmi scelti mediante serializzazione con sentinella di primo e secondo algoritmo con il terzo (\textit{no} per dato normale, \textit{yes} per dato anomalo)
  \item Risultati della combinazione dei tre algoritmi scelti mediante serializzazione con sentinella di secondo e terzo algoritmo con il primo (\textit{no} per dato normale, \textit{yes} per dato anomalo)
\end{enumerate}
Inoltre, nelle colonne $ O - Y $, abbiamo calcolato, partendo dall'alto, il primo e il terzo interquartile di ogni algoritmo, usati per effettuare la conversione dei valori assegnati dagli algoritmi ai singoli dati 
in etichette binarie (come quelle descritte nel precedente elenco), seguiti dal calcolo delle metriche di ogni algoritmo e di ogni sua combinazione.
\newline

Nel descrivere la struttura dei fogli di calcolo utilizzati abbiamo citato gli interquartili, ci\`o perch\`e al fine di rilevare i punteggi anomali all'interno della loro totalit\`a \`e stato necessario affidarci a 
metodi di natura statistica.
\newline

Inizialmente pensavamo di utilizzare una tecnica gi\`a nota per rilevare valori anomali, ovvero quella dello scarto interquartile (un punteggio \`e considerato anomalo se esterno ai seguenti valori soglia: 
$ q1 - 1.5 * iqr $, $ q3 + 1.5 * iqr $, con $q1$ primo interquartile, $q3$ terzo interquartile e $iqr$ differenza tra terzo e primo interquartile), in quanto usata in \cite{25}, tuttavia, con tale tecnica, 
abbiamo riscontrato problemi di consistenza per quel che riguarda il calcolo delle metriche.
\newline

Poich\`e non ci \`e stato possibile calcolare i valori in tabella 1, siamo stati costretti a modificare la tecnica appena citata usando come soglie i soli interquartili.
\newline

Questo ha causato la generazione, nelle tabelle del prossimo capitolo, di valori diversi da quelli della tabella 1.

