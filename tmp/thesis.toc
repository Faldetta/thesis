\babel@toc {italian}{}
\babel@toc {italian}{}
\vspace {-\cftbeforepartskip }
\contentsline {chapter}{\numberline {1}Introduzione}{9}{chapter.1}
\contentsline {chapter}{\numberline {2}Fondamenti}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Anomalie}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Etichette}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Intrusion detection}{13}{section.2.3}
\contentsline {section}{\numberline {2.4}Algoritmi di intrusion detection anomaly based}{14}{section.2.4}
\contentsline {section}{\numberline {2.5}Metriche per la valutazione degli algoritmi}{18}{section.2.5}
\contentsline {section}{\numberline {2.6}Dataset}{21}{section.2.6}
\contentsline {section}{\numberline {2.7}Valutazione degli algoritmi}{22}{section.2.7}
\contentsline {chapter}{\numberline {3}Obiettivo e Metodologia}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Obiettivo}{23}{section.3.1}
\contentsline {section}{\numberline {3.2}Strategie di combinazione}{23}{section.3.2}
\contentsline {section}{\numberline {3.3}Algoritmi}{26}{section.3.3}
\contentsline {section}{\numberline {3.4}Metodologia della fase sperimentale}{28}{section.3.4}
\contentsline {chapter}{\numberline {4}Risultati}{33}{chapter.4}
\contentsline {section}{\numberline {4.1}Algoritmi linearitmici}{34}{section.4.1}
\contentsline {section}{\numberline {4.2}Algoritmi quadratici}{40}{section.4.2}
\contentsline {section}{\numberline {4.3}Generalizzazione}{46}{section.4.3}
\contentsline {chapter}{\numberline {5}Conclusione}{47}{chapter.5}
